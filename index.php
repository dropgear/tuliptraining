<?php
switch ($_GET['p']) {

	case 'quiz':
		require('controllers/quiz.php');
		require('templates/header.html');
		require('templates/take_quiz.html');
		break;

	case 'quizzing':
		require('controllers/verify_quiz.php');
		require('templates/header.html');
		require('templates/static.html');
		break;
		
	case 'lessonviewer':
	$pagetitle="Plaid Awareness";
		require('templates/header.html');
		require('templates/lessonviewer.html');
		break;

	default:
	case 'manager':
		require('controllers/manager.php');
		require('templates/header.html');
		require('templates/newlesson.html');
		require('templates/reports.html');
		break;
}
require('templates/footer.html');
