
<?php
$pagetitle = "Take Quiz";
$quiztitle = "FANDO Plaid Awareness";
$h3font = "uglyh3";
$question_options1 = array("qid"=>"1_0", "value"=>"Yes");
$question_options2 = array("qid"=>"1_1", "value"=>"No");
$question_options3 = array("qid"=>"1_2", "value"=>"Maybe?");
$question_options4 = array("qid"=>"1_3", "value"=>"I Wish");
$question_options[] = $question_options1;
$question_options[] = $question_options2;
$question_options[] = $question_options3;
$question_options[] = $question_options4;

//$question_options = array("1_0"=> "Yes", "1_1"=> "No","1_2"=> "Maybe?","1_3"=> "I Wish");
$question_one = array("question_id"=> "1", "question_text"=>"Which pattern is best for fall?", "question_options"=> $question_options);

$question_options2_1 = array("qid"=>"2_0", "value"=>"Yes");
$question_options2_2 = array("qid"=>"2_1", "value"=>"No");
$question_options2_3 = array("qid"=>"2_2", "value"=>"Maybe?");
$question_options2_4 = array("qid"=>"2_3", "value"=>"I Wish");
$question_options2[] = $question_options2_1;
$question_options2[] = $question_options2_2;
$question_options2[] = $question_options2_3;
$question_options2[] = $question_options2_4;

//$question_options = array("1_0"=> "Yes", "1_1"=> "No","1_2"=> "Maybe?","1_3"=> "I Wish");
$question_two = array("question_id"=> "2", "question_text"=>"What is this?", "question_options"=> $question_options2);

$questions [] = $question_one;
$questions [] = $question_two;

$username = "andrew";